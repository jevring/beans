package net.jevring.beans;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Test {
	public static void main(String[] args) {
		WithAnnotation wa = new WithAnnotation();
		// uncommenting this works perfectly fine on the command line, but of course not in any IDE, as we're using internal compiler apis to mess with the AST.
		// I wish there was some way of doing this in a supported manner.
		// Though I guess value classes will solve most of these issues in the future.
		//String hello = wa.getHello();
		//System.out.println("hello = " + hello);
	}
}
