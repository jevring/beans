package net.jevring.beans;

import net.jevring.beans.annotations.Value;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
@Value
public class WithAnnotation {
	private String hello = "hi there!";
	/*
	private int goodbye;

	public String test(String a, int b) {
		return hello;
	}
	*/
}
