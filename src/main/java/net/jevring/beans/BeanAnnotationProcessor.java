package net.jevring.beans;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import net.jevring.beans.annotations.Value;
import net.jevring.beans.jdk.internal.GetterGenerator;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Collections;
import java.util.Set;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class BeanAnnotationProcessor extends AbstractProcessor {
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		this.processingEnv = processingEnv;
		System.out.println("Annotation processor initialized: " + processingEnv.getClass());
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		if (false) {
			return false;
		}
		Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(Value.class);
		System.out.println("elements = " + elements);
		if (!elements.isEmpty()) {
			Element element = elements.iterator().next();
			System.out.println("element.getClass() = " + element.getClass());
			TreePath path = Trees.instance(processingEnv).getPath(element);
			System.out.println("path.getClass() = " + path.getClass());
			CompilationUnitTree apiCompilationUnit = path.getCompilationUnit();
			System.out.println("path.getCompilationUnit().getClass() = " + apiCompilationUnit.getClass());

			TreeScanner<Object, Object> getterGenerator = GetterGenerator.forProcessingEnvironment(processingEnv);
			apiCompilationUnit.accept(getterGenerator, null);
		}
		return true;
	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return Collections.singleton("net.jevring.beans.annotations.Value");
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.RELEASE_11;
	}
}
