package net.jevring.beans;

import com.sun.source.util.JavacTask;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.source.util.TreeScanner;
import net.jevring.beans.jdk.internal.GetterGenerator;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class BeanPlugin implements com.sun.source.util.Plugin {
	public String getName() {
		return "BeanPlugin";
	}

	public void init(JavacTask task, String... args) {
		System.out.println("BeanPlugin initialized!");


		task.addTaskListener(new TaskListener() {
			@Override
			public void finished(TaskEvent e) {
				if (e.getKind() == TaskEvent.Kind.PARSE) {
					// Invoked once per class (or, technically, probably file or something, including package-info.java etc, but effectively once per class)
					System.out.println("e.getCompilationUnit() = " + e.getCompilationUnit().getSourceFile().getName());

					TreeScanner<Object, Object> getterGenerator = GetterGenerator.forJavaTask(task);
					e.getCompilationUnit().accept(getterGenerator, null);
				}
			}
		});

	}

}
