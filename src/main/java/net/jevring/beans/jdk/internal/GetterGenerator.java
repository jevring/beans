package net.jevring.beans.jdk.internal;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.TreeScanner;
import com.sun.tools.javac.api.BasicJavacTask;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class GetterGenerator extends TreeScanner<Object, Object> {
	private final Names names;
	private final TreeMaker treeMaker;
	private boolean valueClass;
	private ClassTree clazz;

	public GetterGenerator(Names names, TreeMaker treeMaker) {
		this.names = names;
		this.treeMaker = treeMaker;
	}

	public static TreeScanner<Object, Object> forProcessingEnvironment(ProcessingEnvironment processingEnv) {
		if (processingEnv instanceof JavacProcessingEnvironment) {
			JavacProcessingEnvironment javacProcessingEnvironment = (JavacProcessingEnvironment) processingEnv;
			Context context = javacProcessingEnvironment.getContext();
			return forContext(context);
		} else {
			// not supported. Can't do anything
			return new TreeScanner<>();
		}
	}

	public static TreeScanner<Object, Object> forJavaTask(JavacTask task) {
		if (task instanceof BasicJavacTask) {
			BasicJavacTask basicJavacTask = (BasicJavacTask) task;
			Context context = basicJavacTask.getContext();
			return forContext(context);
		} else {
			// not supported. Can't do anything
			return new TreeScanner<>();
		}
	}

	private static TreeScanner<Object, Object> forContext(Context context) {
		TreeMaker treeMaker = TreeMaker.instance(context);
		Names names = Names.instance(context);
		return new GetterGenerator(names, treeMaker);
	}

	@Override
	public Object visitClass(ClassTree node, Object o) {
		clazz = node;
		valueClass = node.getModifiers().getAnnotations().stream().anyMatch(annotation -> "Value".equals(annotation.getAnnotationType().toString()));
		return super.visitClass(node, o);
	}

	@Override
	public Object visitVariable(VariableTree variable, Object o) {
		if (valueClass && variable.getModifiers().getFlags().contains(Modifier.PRIVATE)) {
			// todo: if it's final, or if it's a value class, generate only a getter.
			// todo: if it's a data class and it's not final, generate a setter
			// todo: generate a constructor somewhere
			String fieldName = variable.getName().toString();
			String getterName = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
			Name methodName = names.fromString(getterName);

			JCTree.JCBlock getterBody = treeMaker.Block(0, List.of(treeMaker.Return(treeMaker.Ident(names.fromString(fieldName)))));
			JCTree.JCExpression returnType = (JCTree.JCExpression) variable.getType();
			JCTree.JCMethodDecl getter =
					treeMaker.MethodDef(treeMaker.Modifiers(Flags.PUBLIC), methodName, returnType, List.nil(), List.nil(), List.nil(), getterBody, null);

			// todo: how can I add this method I've created? 
			// or is that not possible?
			// why do these examples lure me with candy, only to turn out to be something completely different =(
			// THE FUCKING TRICK WAS THIS REASSIGNMENT! 
			// https://www.youtube.com/watch?v=xGgZUe7WTBE
			// I had all the fucking thing down, then I rewrote it, painstakingly, to make sure I didn't fuck anything up, and here we are!
			// Such a simple fucking thing!
			((JCTree.JCClassDecl) clazz).defs = ((JCTree.JCClassDecl) clazz).defs.append(getter);
		}
		return super.visitVariable(variable, o);
	}
}
